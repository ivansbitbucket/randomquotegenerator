  "use strict";

    const quotes = [
    "\"   'You cannot shake hands with a clenched fist \"<br>   - Indira Gandhi'",
    "\"  'The Future Belongs To The Competent. Get Good, Get Better, Be The Best!\"<br> - Brian Tracy'",
    "\"'Fake It Until You Make It! Act As If You Had All The Confidence You Require Until It Becomes Your Reality.\"<br>  - Brian Tracy'",
    "\" 'Reading Is To The Mind, As Exercise Is To The Body.\"<br> - Brian Tracy'",
    "\"'To See What Is Right And Not Do It Is A Lack Of Courage.\"<br> - Confucious'",
    "\"'You Are Never Too Old To Set Another Goal Or To Dream A New Dream\"<br>.- C.S. Lewis'",
    "\"'Develop An ‘Attitude Of Gratitude’. Say Thank You To Everyone You Meet For Everything They Do For You.\"<br> - Brian Tracy'",
    "\"'Do What You Can With All You Have, Wherever You Are.\"<br> - Theodore Roosevelt'",
    "\" 'What You Lack In Talent Can Be Made Up With Desire, Hustle And Giving 110% All The Time.\"<br>- Don Zimmer'",
    "\"'Creativity Is Intelligence Having Fun.\"<br>  - Albert Einstein'",
    "\"'The Only Limit To Our Realization Of Tomorrow Will Be Our Doubts Of Today.\"<br>- Franklin D. Roosevelt'",
    "\"'The Man Who Has Confidence In Himself Gains The Confidence Of Others \"<br> - Hasidic Proverb'",
    "\"'Security Is Mostly A Superstition. Life Is Either A Daring Adventure Or Nothing\"<br> .- Helen Keller'",
    "\"'Whether You Think You Can Or Think You Can’t, You’re Right\"<br> - Henry Ford'",
    "\"'Imagine Your Life Is Perfect In Every Respect; What Would It Look Like?\"<br>- Dr. Henry Link'",
    "\"Knowing Is Not Enough; We Must Apply. Wishing Is Not Enough; We Must Do.\"<br> - Johann Wolfgang Von Goethe\"",
    "\" 'Entrepreneurs Are Great At Dealing With Uncertainty And Also Very Good At Minimizing Risk. That’s The Classic Entrepreneur.\"<br>- Maya Angelou'",
    "\"'People Who Are Crazy Enough To Think They Can Change The World, Are The Ones Who Do\"<br> .- Og Mandino',",
    "\" 'If You Are Working On Something That You Really Care About, You Don’t Have To Be Pushed. The Vision Pulls You.\"<br>- Steve Jobs'",
    "\"'It’s Not Whether You Get Knocked Down, It’s Whether You Get Up.\"<br> – Inspirational Quote By Vince Lombardi'",
    "\"The Way Get Started Is To Quit Talking And Begin Doing \" <br>  Walt Disney",
    "\"The Pessimist Sees Difficulty In Every Opportunity. The Optimist Sees Opportunity In Every Difficulty \" <br> Winston Churchill",
    "\"Dude, suckin' at something is the first step at being sorta good at something.\"<br>-  Jake <small><em>(Adventure Time)</em></small>", 
    "\"Don’t Let Yesterday Take Up Too Much Of Today \"<br> Will Rogers",
    "\"You Learn More From Failure Than From Success. Don’t Let It Stop You. Failure Builds Character \"<br>  - Unknown",
    "\"Either I will find a way, or I will make one.\"<br> - Philip Sidney", 
    "\"Our greatest weakness lies in giving up. The most certain way to succeed is always to try just one more time.\"<br>- Thomas A. Edison", 
    "\"You are never too old to set another goal or to dream a new dream.\"<br>- C.S Lewis", 
    "\"If you can dream it, you can do it.\"<br>- Walt Disney",
    "\"Never give up, for that is just the place and time that the tide will turn.\"<br>- Harriet Beecher Stowe",
    "\"I know where I'm going and I know the truth, and I don't have to be what you want me to be. I'm free to be what I want.\"<br>- Muhammad Ali", 
    "\"If you always put limit on everything you do, physical or anything else. It will spread into your work and into your life. There are no limits.<br> There are only plateaus, and you must not stay there, you must go beyond them.\"<br>- Bruce Lee",
  ];
  
  let quoteBtn = document.querySelector(".quoteBtn"),
  quoteDisplay = document.getElementsByTagName('div')[2],
  randomNumber,
  singleQuote = document.getElementsByTagName('p')[0];
  //addQuoteToTwitter
  function addQuoteToTwitter() {
    let socialMediaQuote = quotes[randomNumber].split(' ').join('%20');
    socialMediaQuote.split('<br>').join('');
    socialMediaQuote = "https://twitter.com/intent/tweet?text=" + socialMediaQuote.split('"').join('');
    socialMediaQuote = document.getElementById('socialMediaQuote').setAttribute('href', socialMediaQuote);
  }
  //Generate quote
  quoteBtn.addEventListener('click', function () {
    randomNumber = Math.floor(Math.random() * (quotes.length));
    quoteDisplay.innerHTML = '<p>' + quotes[randomNumber] + '</p>';
    addQuoteToTwitter();
  });
  function startTime() {
    let today = new Date();
    let h = today.getHours();
    let m = today.getMinutes();
    let s = today.getSeconds();
    let ampm = "";
    m = checkTime(m);

    if (h > 12) {
    	h = h - 12;
    	ampm = " PM";
    } else if (h == 12){
        h = 12;
    	ampm = " AM";
    } else if (h < 12){
        ampm = " AM";
    } else {
        ampm = "PM";
    };
  if(h==0) {
    h=12;
  }
  document.querySelector('.display').innerHTML = h+":"+m+ampm;
  let t = setTimeout(function(){startTime()},500);
}
function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
//date
function startDate() {
  let d = new Date();
  let days = ["Sunday"," Monday ","Tuesday","Wednesday","Thursday","Friday"," Saturday "];
  document.querySelector(".date").innerHTML = days[d.getDay()] + " | "+[d.getMonth()+1]+"/"+d.getDate()+"/"+d.getFullYear();
}
